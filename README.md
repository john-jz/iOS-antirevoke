Contains Apple Mobile Configuration File to blacklist servers used for Online Certificate Status Protocol (OCSP), system updates, and various ad servers. <br />

Revokes: ocsp.apple.com <br />
Updates: mesu.apple.com <br />


Credits to AntiRevoke <br />
<https://twitter.com/antirevoke> <br />
<https://antirevoke.lol/> <br />